import { Component, OnInit, ViewChild, ElementRef, Output, EventEmitter, Input } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { LoaderService } from 'src/app/services/loader.service';
import { Subject } from 'rxjs/internal/Subject';
import { takeUntil } from 'rxjs/operators';
import { multilineValue } from 'src/app/models';

@Component({
  selector: 'app-multiline-input',
  templateUrl: './multiline-input.component.html',
  styleUrls: ['./multiline-input.component.scss']
})
export class MultiLineInputComponent implements OnInit {
  public form: FormGroup;
  public loading: boolean;

  private destroyed$ = new Subject();

  @Input() public label: string;
  @Input() public mutedLabel: boolean;
  @Input() public showActions: boolean;
  @Output() private value = new EventEmitter<multilineValue>();
  @ViewChild('multilineInput', { static: false }) private multilineInput: ElementRef;

  constructor(
    private formBuilder: FormBuilder,
    private loader: LoaderService
  ) { }

  ngOnInit() {
    this.getLoaderRequest();
    this.initForm();
  }

  ngOnDestroy() {
    this.destroyed$.next();
    this.destroyed$.complete();
  }

  private getLoaderRequest() {
    this.loader.getStateLoading()
      .pipe(takeUntil(this.destroyed$))
      .subscribe(loading => loading = this.loading);
  }

  private initForm() {
    this.form = this.formBuilder.group({
      multilineInput: [''],
    });
  }

  /**
   * Convenience getter
   * for easy access to form fields
   */
  public get f() {
    return this.form.controls;
  }

  /**
   * Submit multiline value
   */
  public onSubmit() {
    const value: multilineValue = {
      content: this.f.multilineInput.value,
    }

    this.value.emit(value);
    this.form.reset();
    this.autoGrow();
  }

  public autoGrow() {
    const input = this.multilineInput.nativeElement;
    setTimeout(() => {
      input.style.height = `${24}px`;
      input.style.height = `${input.scrollHeight - 60}px`; 
    });
  }

  public onKeyEnter($event) {
    $event.preventDefault();
    this.onSubmit();
  }

}
