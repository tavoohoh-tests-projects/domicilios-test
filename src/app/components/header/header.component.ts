import { Component, Input } from '@angular/core';
import { User } from 'src/app/models';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent {
  @Input() public user: User;

  constructor(
    private authService: AuthService
  ) {}

  public logout() {
    this.authService.logout();
  }
}
