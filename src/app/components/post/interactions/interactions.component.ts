import { Component, OnInit, Input, OnChanges, SimpleChanges, DoCheck } from '@angular/core';
import { Reaction, Post } from 'src/app/models';
import { ReactionType } from 'src/app/enums';

@Component({
  selector: 'app-post-interactions',
  templateUrl: './interactions.component.html',
  styleUrls: ['./interactions.component.scss']
})
export class InteractionsComponent implements DoCheck {
  @Input() public post: Post;

  public reactionsToShow = {
    blue: false,
    red: false,
    yellow: false
  };

  ngDoCheck() {
    this.whatReactionToShow();
  }

  public whatReactionToShow() {
    this.post.reactions.forEach(reaction => {
      if (reaction.type === ReactionType.BLUE) {
        this.reactionsToShow.blue = true;
      }

      if (reaction.type === ReactionType.RED) {
        this.reactionsToShow.red = true;
      }

      if (reaction.type === ReactionType.YELLOW) {
        this.reactionsToShow.yellow = true;
      }

      if (this.reactionsToShow.blue && this.reactionsToShow.red && this.reactionsToShow.yellow) {
        return;
      }
    });
  }

}
