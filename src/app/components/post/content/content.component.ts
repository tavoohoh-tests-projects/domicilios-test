import { Component, Input, Output, EventEmitter } from '@angular/core';
import { Post, Reaction } from 'src/app/models';
import { HelperService } from 'src/app/services/helper.service';
import { ApiService } from 'src/app/services/api.service';
import { ReactionType } from 'src/app/enums';

@Component({
  selector: 'app-post-content',
  templateUrl: './content.component.html',
  styleUrls: ['./content.component.scss']
})
export class ContentComponent {
  @Input() public post: Post;
  @Output() private refresh = new EventEmitter<void>();
  @Output() private comment = new EventEmitter<void>();

  constructor(
    private api: ApiService,
    private helper: HelperService
  ) {}

  public timeSince() {
    return this.helper.timeSince(this.post.createdAt);
  }

  public onReact() {
    const reactionIndex = Math.floor(Math.random() * (2 - 0 + 1));
    const reactions: Array<ReactionType> = [
      ReactionType.BLUE,
      ReactionType.RED,
      ReactionType.YELLOW
    ];
    const reaction: Reaction = {
      type: reactions[reactionIndex]
    };

    this.api.reactionCreate(this.post.id, reaction)
      .subscribe((post: Post) => this.refresh.emit());
  }

  public onComment() {
    this.comment.emit();
  }

}
