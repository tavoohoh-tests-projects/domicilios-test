import { Component, Input, DoCheck } from '@angular/core';
import { Comment } from 'src/app/models';
import { HelperService } from 'src/app/services/helper.service';

@Component({
  selector: 'app-post-comments',
  templateUrl: './comments.component.html',
  styleUrls: ['./comments.component.scss']
})
export class CommentsComponent implements DoCheck{
  @Input() public comments: Array<Comment>;

  constructor(
    private helper: HelperService
  ) { }

  ngDoCheck() {
    // need this to check any change in comments array
  }

  public timeSince(date: string) {
    return this.helper.timeSince(date);
  }
}
