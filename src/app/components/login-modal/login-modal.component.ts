import { Component, Output, EventEmitter, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-login-modal',
  templateUrl: './login-modal.component.html',
  styleUrls: ['./login-modal.component.scss']
})
export class LoginModalComponent implements OnInit {
  public form: FormGroup;
  @Output() private refresh = new EventEmitter<void>();

  constructor(
    private formBuilder: FormBuilder,
    private authService: AuthService
  ) { }

  ngOnInit() {
    this.initForm();
  }

  private initForm() {
    this.form = this.formBuilder.group({
      firstName: ['', [Validators.required]],
      lastName: ['', [Validators.required]],
      email:  ['', [Validators.required, Validators.email]],
    });
  }

  /**
   * Convenience getter
   * for easy access to form fields
   */
  public get f() {
    return this.form.controls;
  }

    /**
   * Submit multiline value
   */
  public onSubmit() {
    if (this.form.invalid) {
      return;
    }

    this.authService.authenticate(this.form.value).subscribe(user => {
      localStorage.setItem('currentUser', user);
      this.form.reset();
      this.refresh.emit();
    });
  }

}
