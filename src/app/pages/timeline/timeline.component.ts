import { Component, OnInit } from '@angular/core';
import { Post, multilineValue, Comment } from 'src/app/models';
import { ApiService } from 'src/app/services/api.service';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { AuthService } from 'src/app/services/auth.service';
import { ReactionType } from 'src/app/enums';

@Component({
  selector: 'app-timeline',
  templateUrl: './timeline.component.html',
  styleUrls: ['./timeline.component.scss']
})
export class TimelineComponent implements OnInit {
  public posts: Array<Post>;

  private destroyed$ = new Subject();

  constructor(
    private auth: AuthService,
    private api: ApiService
  ) { }

  ngOnInit() {
    this.getPosts();
  }

  ngOnDestroy() {
    this.destroyed$.next();
    this.destroyed$.complete();
  }

  public getPosts() {
    this.api.postAll()
      .pipe(takeUntil(this.destroyed$))
      .subscribe((posts: Array<Post>) => {
        this.posts = posts.reverse();

        if (posts.length === 0) {
          this.whenEmptyTimeline();
        }
      });
  }

  public onSubmitPost($value: multilineValue) {
    const post: Post = {
      owner: this.auth.currentUserValue,
      content: $value.content
    };

    this.api.postCreate(post)
      .subscribe((post: Post) => this.getPosts());
  }

  public onSubmitComment(postId: number, $value: multilineValue) {
    const comment: Comment = {
      owner: this.auth.currentUserValue,
      content: $value.content
    };

    this.api.commentCreate(postId, comment)
      .subscribe((post: Post) => this.getPosts());
  }

  public showCommentInput(postIndex: string) {
    this.posts[postIndex].showCommentBox = !this.posts[postIndex].showCommentBox;
  }

  public whenEmptyTimeline() {

    this.api.postCreate({
      owner: {
        firstName: 'Gustavo',
        lastName: 'Santamaría',
        initials: 'GS',
        email: 'ren@mail.com'
      },
      content: 'Hola aquí podras publicar tus estados y otras personas podrán interactuar contigo'
    }).subscribe((post: Post) => {
      this.getPosts();

      setTimeout(() => {
        this.api.reactionCreate(1, { type: ReactionType.BLUE })
        .subscribe((post: Post) => this.getPosts());
      }, 1000);

      setTimeout(() => {
        this.api.commentCreate(1, {
          owner: {
            firstName: 'Rachel',
            lastName: 'Mata',
            initials: 'RM',
            email: 'ray@mail.com'
          },
          content: '¡Tambien podemos comentar en los estados!'
        })
        .subscribe((post: Post) => this.getPosts());
      }, 3000);

      setTimeout(() => {
        this.api.reactionCreate(1, { type: ReactionType.BLUE })
        .subscribe((post: Post) => this.getPosts());
      }, 6000);

      setTimeout(() => {
        this.api.reactionCreate(1, { type: ReactionType.YELLOW })
        .subscribe((post: Post) => this.getPosts());
      }, 7000);

      setTimeout(() => {
        this.api.reactionCreate(1, { type: ReactionType.RED })
        .subscribe((post: Post) => this.getPosts());
      }, 10000);
    });
  }

}
