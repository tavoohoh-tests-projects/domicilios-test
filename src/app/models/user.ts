import { Injectable } from '@angular/core';
import { Adapter } from './adapter';

export class User {
  constructor(
    public firstName: string,
    public lastName: string,
    public email: string,
    public initials?: string
  ) {}
}

@Injectable({
  providedIn: 'root'
})
export class UserAdapter implements Adapter<User> {
  public adapt(item: any): User {
    return new User(
      item.firstName,
      item.lastName,
      item.email,
      this.initials(item.firstName, item.lastName),
    );
  }

  private initials(firstName: string, lastName: string): string {
    const initials = firstName.charAt(0) + lastName.charAt(0);
    return initials.toUpperCase();
  }
}