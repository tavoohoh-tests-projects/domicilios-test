import { ReactionType } from '../enums';
import { User } from './user';
import { Injectable } from '@angular/core';
import { Adapter } from './adapter';

export class Post {
  constructor(
    public owner: User,
    public content: string,
    public comments?: Array<Comment>,
    public reactions?: Array<Reaction>,
    public createdAt?: string,
    public showCommentBox?: boolean,
    public id?: number
  ) {}
}

export interface Comment {
  owner: User,
  content: string;
  createdAt?: string;
}

export interface Reaction {
  type: ReactionType;
}

@Injectable({
  providedIn: 'root'
})
export class PostAdapter implements Adapter<Post> {
  public adapt(item: any): Post {
    return new Post(
      item.owner,
      item.content,
      item.comments,
      item.reactions,
      item.createdAt,
      false,
      item.id
    );
  }
}