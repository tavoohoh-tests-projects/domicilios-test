export enum ReactionType {
  YELLOW = 'yellow',
  BLUE = 'blue',
  RED = 'red'
}