import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { environment as ENV } from '../../environments/environment';
import { Comment, Reaction, Post, PostAdapter } from '../models';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(
    private http: HttpClient,
    private postAdapter: PostAdapter
  ) { }

  public postAll(): Observable<any> {
    const url = `${ENV.apiUrl}/posts/all`;
    return this.http.get(url).pipe(map((data: Array<Post>) => {
      return this.adaptPosts(data)
    }));
  }

  public postCreate(post: Post): Observable<any> {
    const url = `${ENV.apiUrl}/posts/create`;
    return this.http.post(url, post);
  } 

  public commentCreate(postId: number, comment: Comment): Observable<any> {
    const url = `${ENV.apiUrl}/posts/comments/add/${postId}`;
    return this.http.post(url, comment);
  }

  public reactionCreate(postId: number, reaction: Reaction): Observable<any> {
    const url = `${ENV.apiUrl}/posts/reactions/add/${postId}`;
    return this.http.post(url, reaction);
  }

  private adaptPosts(data: Array<any>): Array<Post> {
    let obj = data as any;
    return obj.map((element: Post) => {
      return this.postAdapter.adapt(element);
    });
  }

}
