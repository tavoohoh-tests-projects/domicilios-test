import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LoaderService {
  private loadingRequest$: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);

  public start() {
    this.loadingRequest$.next(true);
  }

  public complete() {
    this.loadingRequest$.next(false);
  }

  public getStateLoading(): Observable<boolean> {
    return this.loadingRequest$.asObservable();
  }
}
