import { Injectable } from '@angular/core';
import { Observable, BehaviorSubject } from 'rxjs';
import { map } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';

import { environment as ENV } from '../../environments/environment';
import { UserAdapter, User } from '../models';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private currentUserSubject: BehaviorSubject<User>;
  public currentUser: Observable<User>;

  constructor(
    private http: HttpClient,
    private userAdapter: UserAdapter
  ) {
    this.currentUserSubject = new BehaviorSubject<User>(JSON.parse(localStorage.getItem('currentUser')));
    this.currentUser = this.currentUserSubject.asObservable();
  }

  private updateUserValue(user: User) {
    const mappedUser = this.userAdapter.adapt(user);
    this.currentUserSubject.next(mappedUser);
    const stringUser = JSON.stringify(mappedUser);
    
    setTimeout(() => {
      localStorage.setItem('currentUser', stringUser);
      return mappedUser;
    }, 500);
  }

  public authenticate(user: User): Observable<any> {
    const url = `${ENV.apiUrl}/users/authenticate`;
    return this.http.post(url, user).pipe(map((data: User) => {
      return this.updateUserValue(data);
    }));
  }
  
  public get currentUserValue(): User {
    return this.currentUserSubject.value;
  }

  public logout() {
    localStorage.removeItem('currentUser');
    this.currentUserSubject.next(null);
    location.reload();
  }

}
