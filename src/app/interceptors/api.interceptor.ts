import { Injectable } from '@angular/core';
import { HttpRequest, HttpResponse, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { Observable, of, throwError } from 'rxjs';
import { delay, mergeMap, materialize, dematerialize } from 'rxjs/operators';
import { Post } from '../models';

const users = JSON.parse(localStorage.getItem('users')) || [];
const posts = JSON.parse(localStorage.getItem('posts')) || [];

@Injectable()
export class ApiInterceptor implements HttpInterceptor {

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const { url, method, headers, body } = request;

    // wrap in delayed observable to simulate server api call
    return of(null)
      .pipe(mergeMap(handleRoute))
      .pipe(materialize())
      // call materialize and dematerialize to ensure delay even if an error is thrown
      // (https://github.com/Reactive-Extensions/RxJS/issues/648)
      .pipe(delay(500))
      .pipe(dematerialize());

    function handleRoute() {
      switch (true) {
        case url.endsWith('/users/authenticate') && method === 'POST':
          return authenticate();
          
        case url.endsWith('/users/me') && method === 'GET':
          return me();

        case url.endsWith('/posts/all') && method === 'GET':
          return statusAll();

        case url.endsWith('/posts/create') && method === 'POST':
          return postCreate();

        case url.includes('/posts/comments/add') && method === 'POST':
          return commentAdd();

        case url.includes('/posts/reactions/add') && method === 'POST':
          return reactionAdd();

        default:
          return next.handle(request);
      }
    }

    function authenticate() {
      const user = users.find(x => x.email === body.email);
      if (!user) {
        return register();
      }
      return ok(user);
    }

    function register() {
      const user = body;

      user.id = users.length ? Math.max(...users.map(x => x.id)) + 1 : 1;
      user.token = Math.random().toString(36).substr(2);
      users.push(user);
      localStorage.setItem('users', JSON.stringify(users));

      return ok(user);
    }

    function me() {
      const user = JSON.parse(localStorage.getItem('currentUser'));
      return ok(user);
    }

    function statusAll() {
      return ok(posts);
    }

    function postCreate() {
      if (!body) {
        return error('status is undefined');
      }

      const post: Post = body;

      post.id = posts.length ? Math.max(...posts.map(x => x.id)) + 1 : 1;
      post.createdAt = new Date().toString();
      post.comments = [];
      post.reactions = [];

      posts.push(post)
      localStorage.setItem('posts', JSON.stringify(posts));

      return ok(post);
    }

    function commentAdd() {
      if (!body) {
        return error('comment is undefined');
      }

      const comment = body;
      comment.createdAt = new Date().toString();
      const postId = url.split('/').reverse()[0];
      const postIndex = posts.findIndex(x => x.id === Number(postId));

      if (postIndex === -1) {
        return error(`Post with id ${postId} was not found`);
      }

      posts[postIndex].comments.push(comment);
      localStorage.setItem('posts', JSON.stringify(posts));

      return ok(posts[postIndex]);
    }

    function reactionAdd() {
      if (!body) {
        return error('reaction is undefined');
      }

      const reaction = body;
      const postId = url.split('/').reverse()[0];
      const postIndex = posts.findIndex(x => x.id === Number(postId));

      if (postIndex === -1) {
        return error(`Post with id ${postId} was not found`);
      }
      
      posts[postIndex].reactions.push(reaction);
      localStorage.setItem('posts', JSON.stringify(posts));

      return ok(posts[postIndex]);
    }

    function ok(bodyContent?: any) {
      return of(new HttpResponse({ status: 200, body: bodyContent }));
    }

    function error(message: any) {
      return throwError({ error: { message } });
    }
  }
}
