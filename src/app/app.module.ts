import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { TimelineComponent } from './pages/timeline/timeline.component';
import { HeaderComponent } from './components/header/header.component';
import { MultiLineInputComponent } from './components/inputs/multiline-input/multiline-input.component';
import { ContentComponent } from './components/post/content/content.component';
import { InteractionsComponent } from './components/post/interactions/interactions.component';
import { CommentsComponent } from './components/post/comments/comments.component';
import { SharedModule } from './modules/shared.module';
import { ApiInterceptor } from './interceptors/api.interceptor';
import { LoginModalComponent } from './components/login-modal/login-modal.component';

const routes: Routes = [
  {
    path: '',
    component: TimelineComponent,
  }
]

@NgModule({
  declarations: [
    AppComponent,
    TimelineComponent,
    HeaderComponent,
    MultiLineInputComponent,
    ContentComponent,
    InteractionsComponent,
    CommentsComponent,
    LoginModalComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(routes),
    HttpClientModule,
    SharedModule
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: ApiInterceptor,
      multi: true
    },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
