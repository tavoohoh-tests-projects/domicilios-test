import { Component, OnInit } from '@angular/core';
import { User } from './models';
import { AuthService } from './services/auth.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styles: []
})
export class AppComponent implements OnInit {
  public currentUser: User;

  constructor(
    private authService: AuthService
  ) {}
  
  ngOnInit() {
    this.checkAuthentication();
  }

  public checkAuthentication() {
    if (this.authService.currentUserValue) {
      this.currentUser = this.authService.currentUserValue;
    }
  }

}
